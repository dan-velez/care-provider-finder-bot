﻿' == Unit Tester ===========================================================
' Run tests on the parser activities for 'Care Provider Finder Bot'.

Imports System.Data.OleDb
Imports CareProviderFinderBotParsers
Imports Excel = Microsoft.Office.Interop.Excel

Module Module1

	Sub Main()
		Console.WriteLine("Test parser code")
		Console.WriteLine()
		' Create a data table for testing
		Dim dtIn As DataTable = New DataTable()
		Dim row As DataRow = dtIn.NewRow() ' Reuse
		' Dim parser = New AetnaParser
		' Dim parser = New AssurantParser
	    ' Dim parser = New BCBSDentalBlueParser
        Dim parser = New EyeMedVisionParser
		' Fill and print the input table
		' TestAetna(dtIn, row)
		' Test Excel Reader
		' PrintDataTable(ReadExcelFile("C:\Users\dev\Documents\UiPath\Care Provider Bot\Data\Assurant\14609.xslx"))
		' TestAssurant(dtIn, row)
		' TestBCBSDentalBlue(dtIn, row)
        TestEyeMedVision(dtIn, row)
		Console.WriteLine("Input DataTable: ")
		PrintDataTable(dtIn)
		' Parse and print the result
		Console.WriteLine()
		Console.WriteLine("Parsed DataTable: ")
		PrintDataTable(parser.ParseTable(dtIn))
		Console.ReadLine()
		' Double.Parse()
	End Sub

	' == Test individual parsers ===============================================
	' Construct the input tables for testing.

	Sub TestAetna(ByRef dtIn As DataTable, ByRef row As DataRow)
		dtIn.Columns.Add("Info")
		dtIn.Columns.Add("Miles")
		row("Info") = "DUANE READE  	40 WALL ST 
NEW YORK, NY 10005- 
(212) 742-8457"
		row("Miles") = "0.0 mi"
		dtIn.Rows.Add(row)
	End Sub

	Sub TestEyeMedVision(ByRef dtIn As DataTable, ByRef row As DataRow)
		dtIn.Columns.Add("C1")
		dtIn.Columns.Add("C2")
		dtIn.Columns.Add("C3")
		dtIn.Columns.Add("C4")
		dtIn.Columns.Add("C5")
		row(0) = "NASSAU FULTON GROUP"
		row(1) = "Products
Eyeglasses, Sunglasses, Contact Lenses, Sports Eyeglasses, Safety Eyeglasses

Services
Exam, Same Day Single Vision, Advanced Digital Exam, Retinal Imaging

Doctors
STUART A FRIEDMAN, O.D."
		row(2) = "87 Nassau St 
New York, NY 10038"
		row(3) = "(212) 233-8735"
		row(4) = "0.3 Miles"
		dtIn.Rows.Add(row)
	End Sub

    Sub TestBCBSDentalBlue(ByRef dtIn As DataTable, ByRef row As DataRow)
        dtIn.Columns.Add("C1")
		dtIn.Columns.Add("C2")
		dtIn.Columns.Add("C3")
		dtIn.Columns.Add("C4")
		row = dtIn.NewRow()
        row(0) = "1. ANN LE
LE ANN N DDS
Center# TX0802-02
5431 BARKER CYPRESS RD STE 100
HOUSTON, TX 77084 
Accepting New Patients: Yes"
        row(1) = "(281)855-2227"
        row(2) = "Dentistry - General"
        row(3) = "1.3 miles   Map & Directions"
        dtIn.Rows.Add(row)
    End Sub

	Sub TestAssurant(ByRef dtIn As DataTable, ByRef row As DataRow)
		dtIn.Columns.Add("C1")
		dtIn.Columns.Add("C2")
		dtIn.Columns.Add("C3")
		dtIn.Columns.Add("C4")
		dtIn.Columns.Add("C5")
		row = dtIn.NewRow()
		' Create first row
		row(0) = "WALL ST. DENTAL
(212)344-9317
Languages Spoken: HINDI PANJABI PERSIAN"
		row(1) = "MEHDI ARYAN
67 WALL ST 
STE 2508 "
		row(2) = "Specialty: General Dentist
NPI: 1922386002
License#: 056258
E-Mail:"
		row(3) = "Open"
		row(4) = "2.3 miles"
		dtIn.Rows.Add(row)
		' Create second row
		row = dtIn.NewRow()
		row(0) = "BEN CORPRON
67 WALL ST 
STE 2508 
NEW YORK, NY 10005-3105"
		row(1) = "Specialty: General Dentist
NPI: 1457727463
License#: 058558
E-Mail: "
		row(2) = "Open"
		row(3) = "0.0 miles"
		dtIn.Rows.Add(row)
		' Create third row
		row = dtIn.NewRow()
		row(0) = "EVAN A LYNN
42 BROADWAY 
STE 1515 
NEW YORK, NY 10004-3884
"
		row(1) = "Specialty: Endodontist
NPI: 1083762363
License#: 049622
E-Mail: 
Board Certified: Y"
		row(2) = "Open"
		row(3) = "0.2 miles"
		dtIn.Rows.Add(row)
		' Create fourth row
		row = dtIn.NewRow()
		row(0) = "SCHEUERMAN, CECILIA U., DMD
(212)385-4666
"
        row(1) = "CECILIA U. SCHEUERMAN
15 MAIDEN LN 
STE 701 
NEW YORK, NY 10038-5120
"
		row(2) = "Specialty: General Dentist
NPI: 1922144690
License#: 041767
E-Mail: 
"
		row(3) = "Open"
		row(4) = "0.2 miles"
		dtIn.Rows.Add(row)
	End Sub

	' == Debugging Utils =======================================================

	Public Sub PrintDataTable(ByVal dtIn As DataTable)
		' Print the header row
		For Each column As DataColumn In dtIn.Columns
			Console.Write("[" & column.ToString() & "], ")
		Next
		Console.WriteLine()
		' Loop through rows
		For Each rowPrint As DataRow In dtIn.Rows
			' Loop through columns
			Console.Write("- ")
			For intCount As Integer = 0 To (dtIn.Columns.Count - 1)
				Console.Write("[" & rowPrint(intCount) & "], ")
			Next
            Console.WriteLine()
			Console.WriteLine("==========================================================================")
		Next
	End Sub

	Private Sub PrintStrArray(ByVal lines As String())
		' Debug lines
		For Each word As String In lines
			Console.WriteLine("Info line: " & word)
		Next
	End Sub

End Module
