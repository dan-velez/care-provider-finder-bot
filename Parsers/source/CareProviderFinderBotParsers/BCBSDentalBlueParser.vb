﻿' == BCBS Dental Blue DataRow Parser =======================================

Imports System.ComponentModel

<DisplayName("BCBSDentalBlueParser DT Parser")>
<Description("Parse the extracted data output from BCBS DentalBlue site")>
Public Class BCBSDentalBlueParser
    Inherits DataTableParser

    Public Overrides Property dictColumns As Dictionary(Of String, Func(Of DataRow, String))

    ' == Column parsers ========================================================

    Public Overrides Sub ConsParsers()
        ' Encapsulate overriding to avoid issues with runtime differences.
        Me.dictColumns = New Dictionary(Of String, Func(Of DataRow, String)) From {
            {
            "Name", Function(ByVal row As DataRow)
                Return Lines(row(0).ToString())(0).Split("."c)(1).Trim()
            End Function
            }, {
            "Organization", Function(ByVal row As DataRow)
                Return Lines(row(0).ToString())(1).Trim()
            End Function
            }, {
            "Address", Function(ByVal row As DataRow)
                Dim lns As String() = Lines(row(0).ToString())
                Return lns(3).Trim() & " " & lns(4).Trim()
            End Function
            }, {
            "Phone", Function(ByVal row As DataRow)
                Return row(1).Trim()
            End Function
            }, {
            "Specialty", Function(ByVal row As DataRow)
                Return row(2).Trim()
            End Function
            }, {
            "Distance", Function(ByVal row As DataRow)
                Return row(3).Split(" "c)(0).Trim()
            End Function
            }
        }
    End Sub

End Class