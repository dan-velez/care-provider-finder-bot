﻿' == EyeMedVision Parser Function ==============================================

Imports System.ComponentModel
Imports System.Text.RegularExpressions

<DisplayName("EyeMedVision DT Parser")>
<Description("Parse the extracted data output from EyeMedVision site")>
Public Class EyeMedVisionParser
    Inherits DataTableParser

    Public Overrides Property dictColumns As Dictionary(Of String, Func(Of DataRow, String))

    ' == Column parsers ========================================================

    Public Overrides Sub ConsParsers()
        ' Encapsulate overriding to avoid issues with runtime differences.
        Me.dictColumns = New Dictionary(Of String, Func(Of DataRow, String)) From {
            {
            "Name", Function(ByVal row As DataRow)
                        Return row(0).ToString
                    End Function
            }, {
            "Doctors", Function(ByVal row As DataRow)
                            Dim lns As String() =  Lines(row(1).ToString())
                            For i As Integer = 0 To lns.Length - 1
                                If lns(i).Equals("Doctors") Then Return lns(i+1).Trim()
                            Next
                           Return ""
                       End Function
            }, {
            "Address", Function(ByVal row As DataRow)
                           Return row(2).ToString
                       End Function
            }, {
            "Phone", Function(ByVal row As DataRow)
                         Return row(3).ToString
                     End Function
            }, {
            "Distance", Function(ByVal row As DataRow)
                Return Regex.Split(row(4).ToString(), "\s+")(0)
            End Function
            }
        }
    End Sub

End Class