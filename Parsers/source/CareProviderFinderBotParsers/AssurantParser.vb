﻿' == Assurant Sun Financial DataRow Parser =====================================

Imports System.ComponentModel

<DisplayName("Assurant DT Parser")>
<Description("Parse the extracted data output from Assurant site")>
Public Class AssurantParser
    Inherits DataTableParser

    Public Overrides Property dictColumns As New Dictionary(Of String, Func(Of DataRow, String))
    
    ' == Column parsers ========================================================

    Public Overrides Sub ConsParsers()
        Me.dictColumns = New Dictionary(Of String, Func(Of DataRow, String)) From {
            {
            "Name", Function(ByVal row As DataRow)
                CheckRowShift(row) ' Fix the row if it needs adjustment.
                Return Lines(row(0).ToString())(0).Trim()
            End Function
            }, {
            "Contact", Function(ByVal row As DataRow)
                CheckRowShift(row)
                Return Lines(row(1).ToString())(0).Trim()
            End Function
            }, {
            "Phone", Function(ByVal row As DataRow)
                CheckRowShift(row)
                Dim lns As String() = Lines(row(0))
                Return Lines(row(0).ToString())(1).Trim()
            End Function
            }, {
            "Languages", Function(ByVal row As DataRow)
                CheckRowShift(row)
                Return Lines(row(0))(2).Split(":"c)(1).Trim()
            End Function
            }, {
            "Address", Function(ByVal row As DataRow)
                CheckRowShift(row)
                Return String.Join(" ", Lines(row(1).ToString()).Skip(1)).Trim()
            End Function
            }, {
            "Specialty", Function(ByVal row As DataRow)
                CheckRowShift(row)
                Return Lines(row(2).ToString())(0).Split(":"c)(1).Trim()
            End Function
            }
        }
    End Sub

    ' == Utils =================================================================
    
    Private Sub CheckRowShift(ByRef row As DataRow)
        ' Some rows are improperly formatted and need to be shifted.
        If row(3).ToString().Trim().EndsWith("miles")
            ' Start at end and assign each precedding value to the current cell.
            For i As Integer = row.ItemArray.Length-1 To 1 Step -1
                row(i) = row(i-1)
            Next
            row(0) = "" ' The first item should be emptied.
        End If
    End Sub

End Class