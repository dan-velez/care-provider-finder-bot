﻿' == Aetna Dental Parser Function ==============================================

Imports System.ComponentModel

<DisplayName("AetnaDental DT Parser")>
<Description("Parse the extracted data output from Aetna Dental site")>
Public Class AetnaParser
    Inherits DataTableParser

    Public Overrides Property dictColumns As Dictionary(Of String, Func(Of DataRow, String))

    ' == Column parsers ========================================================

    Public Overrides Sub ConsParsers()
        ' Encapsulate overriding to avoid issues with runtime differences.
        Me.dictColumns = New Dictionary(Of String, Func(Of DataRow, String)) From {
            {
            "Name", Function(ByVal row As DataRow)
                Return Lines(row(0))(0).Split(vbTab)(0).Trim
            End Function
            }, {
            "Address", Function(ByVal row As DataRow)
                Dim lns As String() = Lines(row(0))
                Dim str As String = lns(0).Split(vbTab)(1).Trim & " " & lns(1).Trim
                Return str.Substring(0, str.Length - 2)
            End Function
            }, {
            "Phone", Function(ByVal row As DataRow)
                Return row(0).Split(ControlChars.CrLf.ToCharArray)(2).Trim
            End Function
            }, {
            "Distance", Function(ByVal row As DataRow)
                Return row(1).Split(Chr(32))(0)
            End Function
            }
        }
    End Sub

End Class