﻿' == DataTableParser ===========================================================
' Inherit this class to create a custom Activity that takes in a DataTable as
' input and returns another DataTable. The actual parser would be implemented
' by overriding the the `dictColumns` property. Each entry in the dictionary is
' a parser for a column which takes the whole row to parse as input.

Imports System.Activities
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Diagnostics
Imports System.Data

<DisplayName("DataTable Parser")>
<Description("Parse a DataTable to return another DataTable.")>
Public Class DataTableParser
    Inherits CodeActivity

    ' Default columns for output table.
    Public Overridable Property dictColumns As Dictionary(Of String, Func(Of DataRow, String))

    ' == Custom WorkFlow Activity Template =====================================

    <Category("Input")>
    <DisplayName("dtInTable")>
    <Description("Supply the DataTable to parse")>
    Public Property DtInTable() As InArgument(Of DataTable)

    <Category("Output")>
    <DisplayName("dtOutTable")>
    <Description("Parsed output will be stored here")>
    Public Property DtOutTable() As OutArgument(Of DataTable)

    Protected Overrides Sub Execute(context As CodeActivityContext)
        Console.WriteLine("DT parser executing")
        ' Retrieve input
        Dim dtIn As DataTable = DtInTable.Get(context)
        ' Set the output DataTable to the parsed version of it.
        DtOutTable.Set(context, Me.ParseTable(dtIn))
        ' This is what the UiPath user will recieve.
    End Sub

    ' == Parser Functions ======================================================

    Public Overridable Sub ConsParsers()
        ' Construct the dictionary of parsers. Override this for custom columns.
    End Sub

    Public Function ParseTable(ByVal dtIn As DataTable) As DataTable
        ' This method is only public for debugging.
        Me.ConsParsers()
        Dim dtResult As DataTable = New DataTable()
        ' Construct the header for the result DataTable.
        For Each elem As KeyValuePair(Of String, Func(Of DataRow, String)) In Me.dictColumns
            dtResult.Columns.Add(elem.Key.ToString)
        Next
        ' Parse each row
        For Each rowIn As DataRow In dtIn.Rows
            Dim rowParsed As DataRow = Me.ParseRow(rowIn, dtResult)
            dtResult.Rows.Add(rowParsed)
        Next
        Return dtResult
    End Function

    Private Function ParseRow(ByRef rowIn As DataRow, ByVal dtIn As DataTable) As DataRow
        Dim rowOut As DataRow = dtIn.NewRow()
        Dim rowInCopy = rowIn ' Use as a reference in Lambda expressions.
        Dim htProcs As Hashtable = New Hashtable() ' Keep the parsing functions here.
        Dim lines As String() = rowIn(0).Split(ControlChars.CrLf.ToCharArray())
        ' ==  Run procedures inside a Try/Catch mechanism. =====================
        For Each elem As KeyValuePair(Of String, Func(Of DataRow, String)) In dictColumns
            Dim strResult = Me.ParseColumn(dictColumns(elem.Key), rowInCopy)
            If strResult.Length = 0 Then
                rowOut(elem.Key) = "N/A"
                Else rowOut(elem.Key) = strResult
            End If
        Next
        Return rowOut
    End Function

    Private Function ParseColumn(f As Func(Of DataRow, String), ByVal row As DataRow) As String
        Try
            Return f(row)
        Catch ex As System.IndexOutOfRangeException
            Console.WriteLine(ex.Message & " " & ex.Source)
            Console.WriteLine(ex.StackTrace)
            Return ""
        End Try
    End Function

    ' == Common Utility Functions ==============================================

    Public Function Lines(ByRef str As String) As String()
        ' Return str.Split(ControlChars.CrLf.ToCharArray)
        Return str.Replace(Chr(13), "").Split(Chr(10))
    End Function

End Class