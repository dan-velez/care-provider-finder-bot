# Description
The Care Provider Finder Bot will scrape from a variety of sources. It is configurable through `settings.json`. It is a small domain specific framework for looping through a list of zip codes and running a search for each one. **All outputed provider data is stored in the `Data/` folder.**

# Configuration

## `dataFile`
This is the file which stores a list of networks and a list of zip codes. Editing this file will alter how the bot works. It will run a special bot for each network listed.

## `networks`
This object contains key/value pairs of a network name and a scraper bot. All networks mentioned in `dataFile` must be present here. The bot will alert you otherwise.

## `botName`
A name for your bot.

## `autorun`
TODO: will decide wether to initiate a selection for each network or run them straight through.

##  `max results per zip`
The maximum number of providers that the bot will scrape per zip code.

## `page timeout`
The maximum number of time (in miliseconds) that the bot will wait before deciding that there are no more pages to scrape. A higher number may catch errors.

# Components

## `Run Scraper`
This component runs the routines that are common to all scrapers, i.e. creating a directory and looping through zip codes. This will open the browser and store a reference to it. The scraper will attach to this reference and extract the data, then restore the browser to its original state. Also conducts all necessary error checking.

## `Scrapers`
A scraper can be created for a certain site and stored in `Scrapers/`. As input it will take a `strZipCode`, and a `browser`. Additionally, for compatability, it will take a `strStartURL`, and a `strNetwork` (the name of the company). They are a single sequence which runs a search for a zip code and extracts the provider information. Every `scraper` will contain a `parser` which is used internally. The parsers are located in the 'Parsers\' folder.

## `Parsers`
Each parser will be an instance of a `DataTableParser`. A `DataTable` parser will expose a function `ParseDataTable` which takes a 
`DataTable` as input and outputs another `DataTable`. The functionality of what happens at each column should be implemented
by overriding a `Dictionary` property called `dictColumns` of type `Dictionary(Of String`/`Function(Of DataRow, String))`.
Each column of the output `DataTable` is a key/value pair where the function returns the result of parsing that column. Each
function should accept the whole row for that column as input.